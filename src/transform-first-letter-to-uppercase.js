const transformFirstLetterToUppercase = (str) => {
  //Your implementaion
  if (typeof str === "string") {
    let reg = /\s+/g;
    let array = str.split(reg);
    array = array.map((e) => {
      return e[0].toUpperCase().concat(e.substr(1));
    });
    return array.join(" ");
  }

  return "Passed argument is not a string";
};

module.exports = transformFirstLetterToUppercase;
